<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
<title>Login</title>
</head>
<body>
	<h1>Fa�a seu Login</h1>
	<c:if test="${param.error}">
		<div>Senha ou Usu�rio Inv�lido.</div>
	</c:if>
	<c:if test="${param.logout}">
		<div>Voc� foi desconectado.</div>
	</c:if>
	<form method="post">
		<div>
			<input name="username" placeholder="Usuario" type="text" />
		</div>
		<div>
			<input name="password" placeholder="Senha" type="password" />
		</div>
		<input type="submit" value="Entrar" />
	</form>
</body>
</html>